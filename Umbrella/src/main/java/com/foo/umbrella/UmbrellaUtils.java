package com.foo.umbrella;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by surabhi on 9/11/17.
 */

public class UmbrellaUtils {

    public static final String CELCIUS = "Celcius";
    public static final String FAHRENHEIT = "Fahrenheit";
    public static final String KEY_ZIPCODE_PREF = "zipcode_preference";
    public static final String KEY_UNIT_PREF = "unit_preference";

    private static final char DEGREE_CHAR = (char) 0x00B0;

    public static final float WARM_COOL_THRESHOLD = 60.0f;

    public static String getFormattedTemp(String temp) {
        return temp + DEGREE_CHAR;
    }

    public static int convertDpToPx(Context context, float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static void setTint(Drawable d, int color) {
        d.setColorFilter(new
                PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
    }

    public static boolean isZipValid(String zip) {
        Pattern p = Pattern.compile("^[0-9]{5}(?:-[0-9]{4})?$");
        Matcher m = p.matcher(zip);
        return m.matches();
    }
}
