package com.foo.umbrella.ui.forecast;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foo.umbrella.R;
import com.foo.umbrella.UmbrellaUtils;
import com.foo.umbrella.data.ApiServicesProvider;
import com.foo.umbrella.data.model.CurrentObservation;
import com.foo.umbrella.data.model.DisplayLocation;
import com.foo.umbrella.data.model.ForecastCondition;
import com.foo.umbrella.data.model.WeatherData;
import com.foo.umbrella.ui.GridSpacingItemDecorator;
import com.foo.umbrella.ui.settings.SettingsFragment;

import org.apache.commons.collections4.CollectionUtils;
import org.threeten.bp.LocalDateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.foo.umbrella.UmbrellaUtils.CELCIUS;
import static com.foo.umbrella.UmbrellaUtils.FAHRENHEIT;
import static com.foo.umbrella.UmbrellaUtils.KEY_UNIT_PREF;
import static com.foo.umbrella.UmbrellaUtils.KEY_ZIPCODE_PREF;
import static com.foo.umbrella.UmbrellaUtils.WARM_COOL_THRESHOLD;
import static com.foo.umbrella.UmbrellaUtils.isZipValid;

/**
 * Created by surabhi on 9/11/17.
 */

public class ForecastFragment extends Fragment {

    @BindView(R.id.city_tv)
    TextView cityTv;
    @BindView(R.id.temp_tv)
    TextView tempTv;
    @BindView(R.id.condition_tv)
    TextView conditionTv;
    @BindView(R.id.settings_btn)
    ImageButton settingsBtn;
    @BindView(R.id.header_container)
    RelativeLayout headerContainer;
    @BindView(R.id.today_card_view)
    CardView todayCardView;
    @BindView(R.id.tomorrow_card_view)
    CardView tomorrowCardView;
    @BindView(R.id.today_hourly_recyclerview)
    RecyclerView todayRecyclerView;
    @BindView(R.id.tomorrow_hourly_recyclerview)
    RecyclerView tomorrowRecyclerView;
    @BindView(R.id.spinner)
    ProgressBar spinner;

    private WeatherData data;
    private String unit;
    private String zip;
    private HourlyAdapter todayHourlyAdapter, tomorrowHourlyAdapter;
    SharedPreferences sharedPref;
    private static final int NUM_COLUMNS_PORTRAIT = 4;
    private static final int NUM_COLUMNS_LANDSCAPE = 6;

    public static ForecastFragment newInstance() {
        return new ForecastFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        //unit default to FAHRENHEIT
        String newUnit = sharedPref.getString(KEY_UNIT_PREF, FAHRENHEIT);
        if (unit == null) {
            unit = newUnit;
        }
        if (!this.unit.equalsIgnoreCase(newUnit)) {
            this.unit = newUnit;
            updateUI();
        }
        zip = sharedPref.getString(KEY_ZIPCODE_PREF, "");
        if (!isZipValid(zip)) {
            promptUserForZipCode();
        } else {
            fetchData();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forecast_fragment, container, false);
        ButterKnife.bind(this, view);
        int margin = (int) getResources().getDimension(R.dimen.margin);
        int columns = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                NUM_COLUMNS_PORTRAIT : NUM_COLUMNS_LANDSCAPE;
        GridSpacingItemDecorator decorator =
                new GridSpacingItemDecorator(margin, columns);

        GridLayoutManager todayGridLayoutManager = new GridLayoutManager(getContext(), columns);
        todayGridLayoutManager.setAutoMeasureEnabled(true);
        todayRecyclerView.addItemDecoration(decorator);
        todayRecyclerView.setNestedScrollingEnabled(false);
        todayRecyclerView.setLayoutManager(todayGridLayoutManager);
        todayHourlyAdapter = new HourlyAdapter(getContext());
        todayRecyclerView.setAdapter(todayHourlyAdapter);

        GridLayoutManager tomorrowGridLayoutManager = new GridLayoutManager(getContext(), columns);
        tomorrowGridLayoutManager.setAutoMeasureEnabled(true);
        tomorrowRecyclerView.addItemDecoration(decorator);
        tomorrowRecyclerView.setNestedScrollingEnabled(false);
        tomorrowRecyclerView.setLayoutManager(tomorrowGridLayoutManager);
        tomorrowHourlyAdapter = new HourlyAdapter(getContext());
        tomorrowRecyclerView.setAdapter(tomorrowHourlyAdapter);
        return view;
    }

    private void fetchData() {
        spinner.setVisibility(View.VISIBLE);
        ApiServicesProvider apiServicesProvider = new ApiServicesProvider(getActivity().getApplication());
        Call<WeatherData> call = apiServicesProvider.getWeatherService().forecastForZipCallable(zip);
        call.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                spinner.setVisibility(View.GONE);
                WeatherData weatherData = response.body();
                if (weatherData != null) {
                    data = weatherData;
                    updateUI();
                }
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                spinner.setVisibility(View.GONE);
                onApiError();
                Log.d(TAG, "API request failed: " + t.getLocalizedMessage());
            }
        });
    }

    private void onApiError() {
        Toast.makeText(getActivity(), getResources().getString(R.string.error_msg_default), Toast.LENGTH_LONG).show();
        UmbrellaUtils.setTint(settingsBtn.getDrawable(), ContextCompat.getColor(getContext(), R.color.text_color_primary));
    }

    private void updateUI() {
        if (data == null) {
            return;
        }
        CurrentObservation currentObservation = data.getCurrentObservation();
        DisplayLocation displayLocation = currentObservation.getDisplayLocation();
        cityTv.setText(displayLocation.getFullName());
        String temp = unit.equals(CELCIUS) ?
                UmbrellaUtils.getFormattedTemp(currentObservation.getTempCelsius()) :
                UmbrellaUtils.getFormattedTemp(currentObservation.getTempFahrenheit());
        tempTv.setText(temp);
        conditionTv.setText(currentObservation.getWeatherDescription());
        UmbrellaUtils.setTint(settingsBtn.getDrawable(), ContextCompat.getColor(getContext(), R.color.text_color_white));
        if (Float.parseFloat(currentObservation.getTempFahrenheit()) >= WARM_COOL_THRESHOLD) {
            headerContainer.setBackgroundResource(R.color.weather_warm);
        } else {
            headerContainer.setBackgroundResource(R.color.weather_cool);
        }
        updateHourlyForecast();
    }

    private void updateHourlyForecast() {
        if (CollectionUtils.isEmpty(data.getForecast())) {
            return;
        }
        List<ForecastCondition> todayForecasts = new ArrayList<>();
        List<ForecastCondition> tomorrowForecasts = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int today = calendar.get(Calendar.DAY_OF_MONTH);
        // dividing hourly forecast into today & tomorrow
        for (ForecastCondition fc : data.getForecast()) {
            LocalDateTime dateTime = fc.getDateTime();
            if (dateTime.getDayOfMonth() == today) {
                todayForecasts.add(fc);
            } else if (dateTime.getDayOfMonth() == today + 1) {
                tomorrowForecasts.add(fc);
            }
        }
        todayHourlyAdapter.setDataSet(todayForecasts, unit);
        tomorrowHourlyAdapter.setDataSet(tomorrowForecasts, unit);
        tomorrowCardView.setVisibility(View.VISIBLE);
        todayCardView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.settings_btn)
    public void onSettingsClick() {
        SettingsFragment settingsFragment = SettingsFragment.newInstance();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, settingsFragment)
                .addToBackStack(null)
                .commit();
    }

    private void promptUserForZipCode() {
        Resources res = getResources();
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        final EditText edittext = new EditText(getActivity());
        edittext.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        edittext.setMaxLines(1);
        alert.setMessage(res.getString(R.string.enter_a_zipcode));
        alert.setView(edittext);
        alert.setCancelable(false); // to force the dialog keep on showing and not dismiss
        alert.setPositiveButton(res.getString(R.string.save), (DialogInterface dialog, int whichButton) ->
                saveZipCode(edittext.getText().toString()));

        alert.show();
    }

    private void saveZipCode(String zipcode) {
        this.zip = zipcode;
        if (isZipValid(zip)) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(KEY_ZIPCODE_PREF, zipcode);
            editor.commit();

            fetchData();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.invalid_zip), Toast.LENGTH_LONG).show();
            promptUserForZipCode();
        }
    }
}
