package com.foo.umbrella.ui.forecast;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foo.umbrella.R;
import com.foo.umbrella.UmbrellaUtils;
import com.foo.umbrella.data.model.ForecastCondition;
import com.squareup.picasso.Picasso;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;
import static com.foo.umbrella.UmbrellaUtils.CELCIUS;

/**
 * Created by surabhi on 9/11/17.
 */

public class HourlyAdapter extends RecyclerView.Adapter<HourlyAdapter.ViewHolder> {
    private List<ForecastCondition> dataSet;
    private Context context;
    private String unit;
    private int lowTempPosition;
    private int highTempPosition;

    private static int warmColor;
    private static int coolColor;
    private static int defaultColor;

    public HourlyAdapter(Context context) {
        this.context = context;
        warmColor = ContextCompat.getColor(context, R.color.weather_warm);
        coolColor = ContextCompat.getColor(context, R.color.weather_cool);
        defaultColor = ContextCompat.getColor(context, R.color.text_color_primary);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.time_tv)
        TextView timeTv;
        @BindView(R.id.temp_tv)
        TextView tempTv;
        @BindView(R.id.icon_iv)
        ImageView iconIv;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public void setDataSet(List<ForecastCondition> dataSet, String unit) {
        if (CollectionUtils.isEmpty(dataSet)) {
            return;
        }
        this.dataSet = dataSet;
        this.unit = unit;
        updateHighLowPositions();
        notifyDataSetChanged();
    }

    @Override
    public HourlyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hourly_forecast_item_view, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(HourlyAdapter.ViewHolder holder, final int position) {
        ForecastCondition forecastCondition = dataSet.get(position);
        if (forecastCondition != null) {
            int color = getColorForPosition(position);

            holder.timeTv.setTextColor(color);
            holder.timeTv.setText(forecastCondition.getDisplayTime());

            String temp = unit.equals(CELCIUS) ?
                    UmbrellaUtils.getFormattedTemp(forecastCondition.getTempCelsius()) :
                    UmbrellaUtils.getFormattedTemp(forecastCondition.getTempFahrenheit());
            holder.tempTv.setTextColor(color);
            holder.tempTv.setText(temp);

            Drawable drawable = getIconDrawable(forecastCondition.getIcon(), color);
            // find local drawable resource for condition, if not, fallback to api icon
            if (drawable != null) {
                holder.iconIv.setImageDrawable(drawable);
            } else {
                Picasso.with(context).load(forecastCondition.getIconUrl()).into(holder.iconIv);
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    private Drawable getIconDrawable(String condition, int color) {
        Drawable d = null;
        int resId = context.getResources()
                .getIdentifier("weather_" + condition, "drawable", context.getPackageName());
        try {
            d = ContextCompat.getDrawable(context, resId);
            d.setColorFilter(new
                    PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
        } catch (Resources.NotFoundException e) {
            Log.d(TAG, "local icon resource not found for current condition");
        } finally {
            return d;
        }
    }

    // find index positions of lowest and highest temp during the day
    private void updateHighLowPositions() {
        highTempPosition = 0;
        lowTempPosition = 0;
        float maxTempSoFar = Float.parseFloat(dataSet.get(0).getTempFahrenheit());
        float minTempSoFar = Float.parseFloat(dataSet.get(0).getTempFahrenheit());
        for (int i = 1; i < dataSet.size(); i++) {
            float temp = Float.parseFloat(dataSet.get(i).getTempFahrenheit());
            if (temp > maxTempSoFar) {
                maxTempSoFar = temp;
                highTempPosition = i;
            }
            if (temp < minTempSoFar) {
                minTempSoFar = temp;
                lowTempPosition = i;
            }
        }
    }

    // return cool/warm color based on hourly item position
    private int getColorForPosition(int position) {
        int color = defaultColor;
        if (highTempPosition != lowTempPosition) {
            if (position == highTempPosition) {
                color = warmColor;
            } else if (lowTempPosition == position) {
                color = coolColor;
            }
        }
        return color;
    }
}
