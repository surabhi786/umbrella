package com.foo.umbrella.ui.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.foo.umbrella.R;
import com.foo.umbrella.UmbrellaUtils;

import static com.foo.umbrella.UmbrellaUtils.KEY_ZIPCODE_PREF;

/**
 * Created by surabhi on 9/11/17.
 */

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener{
    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        if (toolbar != null) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //register the preferenceChange listener
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        //unregister the preference change listener
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Load the Preferences from the XML file
        addPreferencesFromResource(R.xml.preferences);
        Preference zipcodePref = findPreference(KEY_ZIPCODE_PREF);
        updateSummary(zipcodePref);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);
        if (KEY_ZIPCODE_PREF.equalsIgnoreCase(key)) {
            updateSummary(preference);
        }
    }

    private void updateSummary(Preference preference) {
        android.support.v7.preference.EditTextPreference editTextPreference = (android.support.v7.preference.EditTextPreference) preference;
        String zip = editTextPreference.getText();
        if (UmbrellaUtils.isZipValid(zip)) {
            editTextPreference.setSummary(zip);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.invalid_zip), Toast.LENGTH_LONG).show();
        }

    }
}
